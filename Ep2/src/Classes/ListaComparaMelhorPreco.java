/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gobellek
 */
public class ListaComparaMelhorPreco {
        // Declaracao
    private final List<ObjMelhorPreco> lista_compara_melhor_preco;
    
    
        // Construtor
    public ListaComparaMelhorPreco(){
        this.lista_compara_melhor_preco = new ArrayList<>();
    }
    
    
        // Metodos e funcoes auxiliares
            //Adicional veiculo na lista
    public void add(String tipo, float preco, float tempo_de_viagem){
        ObjMelhorPreco c = new ObjMelhorPreco(tipo, preco, tempo_de_viagem);
        this.lista_compara_melhor_preco.add(c);
    }
    
            // Verifica o tamnho da lista
    public int listSize(){
        return this.lista_compara_melhor_preco.size();
    }
            // Retorna preco do objeto em determinada posicao da lista
    public float getObjMelhorPreco(int a){
        return this.lista_compara_melhor_preco.get(a).getPreco();   
    }
            // Retorna tipo do objeto em determinada posicao da lista
    public String getTipoObjMelhorPreco(int a){
        return this.lista_compara_melhor_preco.get(a).getTipo();
    }
            // Retorna o tempo do objeto em determinada posicao da lista
    public float getTempoObjMelhorPreco(int a){
        return this.lista_compara_melhor_preco.get(a).getTempoDeViagem();
    }
    
            // Retorna o objeto em determinada posicao da lista
    public ObjMelhorPreco showMelhorPreco(int a){
        System.out.println(this.lista_compara_melhor_preco.get(a).getTipo());
        System.out.println(this.lista_compara_melhor_preco.get(a).getPreco());
        return this.lista_compara_melhor_preco.get(a);
    }
   
    
}
