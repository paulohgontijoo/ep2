/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author gobellek
 */
public class ObjMelhorPreco {

    private String tipo;
    private float preco;
    private float tempo_de_viagem;
    
   
    public ObjMelhorPreco(String tipo, float preco, float tempo_de_viagem){
        this.tipo = tipo;
        this.preco = preco;
        this.tempo_de_viagem = tempo_de_viagem;
    }
    
    
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }
    
    public float getTempoDeViagem() {
        return tempo_de_viagem;
    }

    public void setTempoDeViagem(float tempo) {
        this.tempo_de_viagem = tempo_de_viagem;
    }
    
}
