/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author gobellek
 */
public class Carreta extends Veiculos{
    
    // Construtor da Carreta
    
    public Carreta(){
        setTipo("Carreta");
        setCombustivel("Diesel");
        setRendimento(8.0f);
        setCarga_max(30000.0f);
        setVel_med(60.0f); 
        setR_rendimento(0.0002);
    }
}
