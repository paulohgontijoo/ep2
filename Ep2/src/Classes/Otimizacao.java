/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gobellek
 */

    //Classe responsavel pelas funcoes de otimizacao e decisao dos fretes
public class Otimizacao {

    
    public float MenorPreco(float carga, float distancia, float tempo, float lucro, float red_rendimento, float rendimento, float preco_combustivel){
        float rend_peso, rend_result, custo, preco;
        
        rend_peso = carga * red_rendimento;
        rendimento = rendimento - rend_peso;
        rend_result = distancia / rendimento;
        custo = rend_result * preco_combustivel;
        preco = custo * (1+(lucro/100));
        
        return preco;
    }
    
    public float TempoDeViagem(float vel_med, float distancia){
        float tempo_de_viagem;
        tempo_de_viagem = distancia / vel_med;
        return tempo_de_viagem;
    }
    
    
    
    
    
}
