/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author gobellek
 */
public class Moto extends Veiculos {
            
    //  Construtor do Moto
    public Moto(){
        setTipo("Moto");
        setCombustivel("Gasolina/Alcool");
        setRendimento(50.0f);
        setCarga_max(50.0f);
        setVel_med(110.0f);
        
    }
    
}
