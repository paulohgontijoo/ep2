/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author gobellek
 */
public class FreteTableModel extends AbstractTableModel{
    private final List<Frete> dados_frete;
    private String[] colunas_frete = {"Carga","Distancia","Tempo","Lucro"};

    public FreteTableModel() {
        this.dados_frete = new ArrayList<>();
    }

    public String getColumnName(int column) {
        return colunas_frete[column]; 
    }
    
    

    public int getRowCount() {
        return dados_frete.size();
    }

    public int getColumnCount() {
        return  colunas_frete.length;
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        switch(coluna){
            case 0:
                return dados_frete.get(linha).getCarga();
            case 1:
                return dados_frete.get(linha).getDistancia();
            case 2:
                return dados_frete.get(linha).getTempo();
            case 3:
                return dados_frete.get(linha).getLucro();
        }
        
        return null;
    }
    
    public void addRow(Frete f){
        this.dados_frete.add(f);
        this.fireTableDataChanged();
    }
    
    public void removeRow(int linha){
        this.dados_frete.remove(linha);
        this.fireTableDataChanged();
    }
    
}
