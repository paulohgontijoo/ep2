/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author gobellek
 */
public class Van extends Veiculos{
    
    //  Construtor da Van 
    public Van(){
        setTipo("Van");
        setCombustivel("Diesel");
        setRendimento(10.0f);
        setCarga_max(3500.0f);
        setVel_med(80.0f);
        setR_rendimento(0.0001);
        
    }
}
