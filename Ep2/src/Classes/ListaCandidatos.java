/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gobellek
 */
public class ListaCandidatos {

    private final List<Veiculos> lista_candidatos;
    
    public ListaCandidatos(){
        this.lista_candidatos = new ArrayList<>();
    }
    
    public void add(Veiculos v){
        this.lista_candidatos.add(v);
    }
    
    public int listSize(){
        return this.lista_candidatos.size();
    }
    
    public Object getValueAt(int linha, int coluna) {
        switch(coluna){
            case 0:
                return lista_candidatos.get(linha).getTipo();
            case 1:
                return lista_candidatos.get(linha).getCombustivel();
            case 2:
                return lista_candidatos.get(linha).getRendimento();
            case 3:
                return lista_candidatos.get(linha).getCarga_max();
            case 4:
                return lista_candidatos.get(linha).getVel_med();
            case 5:
                return lista_candidatos.get(linha).getR_rendimento();
        }
        
        return null;
    }
    
}
