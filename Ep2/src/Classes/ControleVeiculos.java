/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;

/**
 *
 * @author gobellek
 */
public class ControleVeiculos {
        // Declaracao de uma lista de veiculos
    private final ArrayList<Veiculos> listaVeiculos;
    
        // Construtor
    public ControleVeiculos(){
        this.listaVeiculos = new ArrayList<Veiculos>();
    }
        // Mostrar lista 
    public ArrayList<Veiculos> getListaVeiculos(){
        return listaVeiculos;
    }
        // Adicionar
    public void adicionarVeiculos(Veiculos umVeiculo){
        listaVeiculos.add(umVeiculo);
    }
        //Remover
    public void removerVeiculos(Veiculos umVeiculo){
        listaVeiculos.remove(umVeiculo);
    }

    public Veiculos pesquisar(String tipo){
        for(Veiculos b: listaVeiculos){
            if(b.getTipo().equalsIgnoreCase(tipo)) return b;
    }
        return null;
    }
}
    
 
