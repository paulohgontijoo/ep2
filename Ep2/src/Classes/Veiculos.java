/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;

/**
 *
 * @author gobellek
 */

// Implementacao da classe Veiculos
public class Veiculos {
    
        // Atributos privados
    private String tipo;
    private String combustivel;
    private float rendimento;
    private float carga_max;
    private float vel_med;
    private double r_rendimento;
    
        // Construtor
    public Veiculos(){
        
        setTipo("Veiculo");
        setCombustivel("Combustivel");
        setRendimento(0.0f);
        setCarga_max(0.0f);
        setVel_med(0.0f);
        setR_rendimento(0.0);
    }
    
    // Definicao dos Metodos (Get e Set)
        
        //Tipo
    public String getTipo() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo = tipo;   
    }

        //Combustivel
    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

        //Rendimento
    public float getRendimento() {
        return rendimento;
    }

    public void setRendimento(float rendimento) {
        this.rendimento = rendimento;
    }

        // Carga Maxima
    public float getCarga_max() {
        return carga_max;
    }

    public void setCarga_max(float carga_max) {
        this.carga_max = carga_max;
    }

        // Velocidade Media
    public float getVel_med() {
        return vel_med;
    }

    public void setVel_med(float vel_med) {
        this.vel_med = vel_med;
    }

        // Reducao de rendimento
    public double getR_rendimento() {
        return r_rendimento;
    }

    public void setR_rendimento(double r_rendimento) {
        this.r_rendimento = r_rendimento;
    }
       
    
}
