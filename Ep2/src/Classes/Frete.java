/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author gobellek
 */
public class Frete {

            // Atributos
        private float carga;
        private float distancia;
        private float tempo;
        private float lucro;

        
            // Metodos
        public Frete(){
        }
        
        //Carga
        public float getCarga(){
            return carga;
        }
        
        public void setCarga(float carga){
            this.carga = carga;
        }
        
        //Distancia
        public float getDistancia(){
            return distancia;
        }
        
        public void setDistancia(float distancia){
            this.distancia = distancia;
        }
        
        //Tempo
        public float getTempo(){
            return tempo;
        }
        
        public void setTempo(float tempo){
            this.tempo = tempo;
        }
        
        //Lucro
        public float getLucro() {
            return lucro;
        }

        public void setLucro(float lucro) {
            this.lucro = lucro;
        }
        
}
