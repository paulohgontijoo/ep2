/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gobellek
 */
public class ListaComparaMaisRapido {
    
        private final List<ObjMelhorPreco> lista_compara_mais_rapido;
        
        public ListaComparaMaisRapido(){
            this.lista_compara_mais_rapido = new ArrayList<>();
        }
        
                    // Metodos e funcoes auxiliares
                //Adicional veiculo na lista
        public void add(String tipo, float preco,float tempo_de_viagem){
            ObjMelhorPreco c = new ObjMelhorPreco(tipo, preco, tempo_de_viagem);
            this.lista_compara_mais_rapido.add(c);
        }

                // Verifica o tamnho da lista
        public int listSize(){
            return this.lista_compara_mais_rapido.size();
        }
                // Retorna preco do objeto em determinada posicao da lista
        public float getPrecoObjMelhorTempo(int a){
            return this.lista_compara_mais_rapido.get(a).getPreco();   
        }
                // Retorna tipo do objeto em determinada posicao da lista
        public String getTipoObjMelhorTempo(int a){
            return this.lista_compara_mais_rapido.get(a).getTipo();
        }
                // Retorna o tempo do objeto em determinada posicao da lista
        public float getTempoObjMelhorTempo(int a){
            return this.lista_compara_mais_rapido.get(a).getTempoDeViagem();
        }

                // Retorna o objeto em determinada posicao da lista
        public ObjMelhorPreco showMelhorTempo(int a){
            System.out.println(this.lista_compara_mais_rapido.get(a).getTipo());
            System.out.println(this.lista_compara_mais_rapido.get(a).getPreco());
            System.out.println(this.lista_compara_mais_rapido.get(a).getTempoDeViagem());
            return this.lista_compara_mais_rapido.get(a);
     }

}
