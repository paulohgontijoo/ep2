/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author gobellek
 */
public class VeiculosTableModel extends AbstractTableModel {
    
    private final List<Veiculos> dados;
    private String[] colunas = {"Tipo","Combustível","Rendimento","Carga Máxima","Velocidade Média"};

    public VeiculosTableModel() {
        this.dados = new ArrayList<>();
    }

    @Override
    public String getColumnName(int column) {
        return colunas[column]; //To change body of generated methods, choose Tools | Templates.
    }
    
    

    @Override
    public int getRowCount() {
        return dados.size();
    }

    @Override
    public int getColumnCount() {
        return  colunas.length;
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        switch(coluna){
            case 0:
                return dados.get(linha).getTipo();
            case 1:
                return dados.get(linha).getCombustivel();
            case 2:
                return dados.get(linha).getRendimento();
            case 3:
                return dados.get(linha).getCarga_max();
            case 4:
                return dados.get(linha).getVel_med();
            case 5:
                return dados.get(linha).getR_rendimento();
        }
        
        return null;
    }
    
    /**
     *
     * @param c
     */
    public void addRow(Veiculos c){
        this.dados.add(c);
        this.fireTableDataChanged();
    }
    
    public void removeRow(int linha){
        this.dados.remove(linha);
        this.fireTableDataChanged();
    }

    public Veiculos getObject(int a){
        return this.dados.get(a); 
    }
    
    public boolean size() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
